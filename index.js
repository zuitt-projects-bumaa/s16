// alert("hi!!") 

function displayMsgToSelf() {
	console.log("Congratulations, Web Dev!")
};
/*
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
*/

let count = 10;

while(count !== 0) {
	displayMsgToSelf();
	count --;
};

// While Loop - allows us to repeat an instruction as long as condition is true
		// do not use Incrementation so your browser won't crash
		/*
		Syntax:
		while(condition is true) {
			statement/ instruction runs

		}
		*/

let number = 5 

//countdown
while(number !== 0) {
	// The current value will be printed out
	console.log("While" + " " + number);
	number--;
};

//count up 5-10
while(number <= 10) {
	console.log(`While ${number}`);
	number++;
};


// Do-While Loop
	// works a lot like a While- Loop 
	// but unlike while, do-while guarantees to run atleast once
	/*
	Syntax: 

	*/


let number2 = Number(prompt("Give me a number")
	)
do{
	// the current value of number2 is printed out
	console.log(`Do While ${number2}`);

	// increases the number by 1 after every iteration
	number += 1;

} while(number < 10);



let counter = 1

do {
	console.log(counter);
	counter++
} while (counter <=21);



// For Loop 
	// a more flexible loop than while and do-while
	// consits of 3 parts: initialization, condition, final expression
		// initialization determines where the loop starts
		// condition determines where the loop stops
		// final exp. indicates how to advance the loop (inc or dec)
	/*
	Syntax:
	for(initialization; condition; finalExpression){
		console.log
	};
	*/

for(let count = 0; count <= 20; count++){
	console.log(count)
};


let myString = "alex"
console.log(myString.length)
	//result: 4
	// note: strings are special, compared to aother data types
		// it has access to functions and other pieces of information 
		// other primitive types might not have

	// myString = ["a", "l", "e", "x"]

	console.log(myString[0]);
		// result: a
	console.log(myString[3]);
		// result: x

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	};


//If condition inside a for-loop

let myName = "Emmalyn"

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"

		){
		console.log(3)
	} else {
		console.log(myName[i])
	}
};

// Continue and Break Statements
	//


for(let count = 0; count <= 20; count++) {
	if(count %2 === 0) {
		continue;
	}
	console.log(`Continue and Break ${count}`)

	if(count > 10) {
		break;
	}
};



let name = "alexandro";

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration")
		continue;
	}
	if(name[i] == "d") {
		break;
	}
};